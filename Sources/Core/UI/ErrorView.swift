//
//  ErrorView.swift
//  
//
//  Created by Luis Emilio Dias Wolf on 11/03/21.
//

import UIKit

public class ErrorView: UIView {
    
    public weak var delegate: ErrorViewDelegate? {
        didSet {
            if delegate != nil {
                stackView.addArrangedSubview(tryAgainButton)
            }
        }
    }
    
    var message: String? {
        didSet {
            messageLabel.text = message
        }
    }
    
    private lazy var stackView: UIStackView = {
        let view = UIStackView()
        view.axis = .vertical
        view.alignment = .center
        view.distribution = .fill
        view.spacing = 16
        view.addArrangedSubview(imageView)
        view.addArrangedSubview(messageLabel)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var imageView: UIImageView = {
        let view = UIImageView(image: UIImage(named: "error", in: Bundle.module, compatibleWith: nil))
        view.contentMode = .scaleAspectFit
        view.tintColor = .appOrange
        return view
    }()
    
    private lazy var messageLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 14.0)
        label.numberOfLines = 0
        label.textAlignment = .center
        return label
    }()
    
    private lazy var tryAgainButton: UIButton = {
        let button = UIButton()
        button.setTitle(NSLocalizedString("try_again", bundle: .module, comment: String()), for: .normal)
        button.setTitleColor(.appGray, for: .normal)
        button.titleLabel?.font = .systemFont(ofSize: 14.0)
        button.addTarget(self, action: #selector(tryAgain), for: .touchUpInside)
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        return nil
    }
    
}

// MARK: - Setup
extension ErrorView {
    
    private func commonInit() {
        addSubview(stackView)
        autoLayout()
    }

    private func autoLayout() {
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: topAnchor),
            stackView.bottomAnchor.constraint(equalTo: bottomAnchor),
            stackView.leadingAnchor.constraint(equalTo: leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: trailingAnchor)
        ])
    }
    
}

// MARK: - Actions
extension ErrorView {
    
    @objc
    public func tryAgain() {
        delegate?.didTryAgain()
    }
    
}
