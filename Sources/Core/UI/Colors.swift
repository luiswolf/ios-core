//
//  Colors.swift
//  
//
//  Created by Luis Emilio Dias Wolf on 20/03/21.
//

import UIKit


extension UIColor {
    
    class func getColor(named name: String) -> UIColor? {
        guard
            #available(iOS 11.0, *),
            let assetColor = UIColor(named: name, in: Bundle.module, compatibleWith: nil)
        else {
            return nil
        }
        return assetColor
    }
    
    static let appBlack: UIColor = getColor(named: "Black") ?? #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    static let appGray: UIColor = getColor(named: "Gray") ?? #colorLiteral(red: 0.3725490196, green: 0.3725490196, blue: 0.3725490196, alpha: 1)
    static let appOrange: UIColor = getColor(named: "Orange") ?? #colorLiteral(red: 1, green: 0.5764705882, blue: 0, alpha: 1)
    static let appWhite: UIColor = getColor(named: "White") ?? #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    
}
