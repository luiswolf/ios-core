//
//  File.swift
//  
//
//  Created by Luis Emilio Dias Wolf on 12/03/21.
//

import UIKit

public class BasicTableViewCell: UITableViewCell, CellIdentifier {

    @objc public dynamic var customFont: UIFont?
    @objc public dynamic var customTextColor: UIColor?
    
    public convenience init(withText text: String) {
        self.init()
        textLabel?.text = text
        textLabel?.numberOfLines = 0
        selectionStyle = .none
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .default, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder: NSCoder) {
        return nil
    }
    
    public override func didMoveToWindow() {
        super.didMoveToWindow()
        textLabel?.font = customFont
        textLabel?.textColor = customTextColor
    }

}
