//
//  LoaderProtocol.swift
//  
//
//  Created by Luis Emilio Dias Wolf on 11/03/21.
//

import UIKit

public protocol LoaderProtocol: UIViewController {
    
    func showLoader()
    
    func hideLoader()
    
}


// MARK: - Default
extension LoaderProtocol {
    
    public func showLoader() {
        hideLoader()
    
        let container = ContainerView<LoaderView>()
        container.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(container)
        
        let margin = view.layoutMarginsGuide
        
        NSLayoutConstraint.activate([
            container.topAnchor.constraint(equalTo: margin.topAnchor, constant: -LayoutHelper.safeArea.top),
            container.bottomAnchor.constraint(equalTo: margin.bottomAnchor, constant: LayoutHelper.safeArea.bottom),
            container.leadingAnchor.constraint(equalTo: margin.leadingAnchor, constant: -24.0),
            container.trailingAnchor.constraint(equalTo: margin.trailingAnchor, constant: 24.0)
        ])
    }
    
    public func hideLoader() {
        view.subviews.first(where: { $0 is ContainerView<LoaderView>})?.removeFromSuperview()
    }
    
}

