//
//  Identifiable.swift
//  
//
//  Created by Luis Emilio Dias Wolf on 25/03/21.
//

import Foundation

public protocol Identifiable {
    
    static var identifier: String { get }
    
}

// MARK: - Default
public extension Identifiable {

    static var identifier: String { String(describing: Self.self) }

}
