//
//  ErrorViewDelegate.swift
//  
//
//  Created by Luis Emilio Dias Wolf on 20/03/21.
//

import Foundation

public protocol ErrorViewDelegate: class {
    
    func didTryAgain()
    
}

