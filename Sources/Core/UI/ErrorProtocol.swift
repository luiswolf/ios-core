//
//  ErrorProtocol.swift
//  
//
//  Created by Luis Emilio Dias Wolf on 11/03/21.
//

import UIKit

public protocol ErrorProtocol: UIViewController {
    
    func showError(withMessage message: String)
    
    func hideError()
    
    var errorDelegate: ErrorViewDelegate? { get }
    
}

// MARK: - Default
public extension ErrorProtocol {
    
    var errorDelegate: ErrorViewDelegate? { nil }
    
    func showError(withMessage message: String) {
        hideError()
    
        let container = ContainerView<ErrorView>()
        container.subview.delegate = errorDelegate
        container.subview.message = message
        container.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(container)
        
        let margin = view.layoutMarginsGuide
        
        NSLayoutConstraint.activate([
            container.topAnchor.constraint(equalTo: margin.topAnchor, constant: -LayoutHelper.safeArea.top),
            container.bottomAnchor.constraint(equalTo: margin.bottomAnchor, constant: LayoutHelper.safeArea.bottom),
            container.leadingAnchor.constraint(equalTo: margin.leadingAnchor, constant: -24.0),
            container.trailingAnchor.constraint(equalTo: margin.trailingAnchor, constant: 24.0)
        ])
    }
    
    func hideError() {
        view.subviews.first(where: { $0 is ContainerView<ErrorView>})?.removeFromSuperview()
    }
    
}

