//
//  LayoutHelper.swift
//  
//
//  Created by Luis Emilio Dias Wolf on 27/03/21.
//

import UIKit

class LayoutHelper {
    
    class var safeArea: UIEdgeInsets {
        guard #available(iOS 11, *) else { return .zero }
        return UIApplication.shared.keyWindow?.safeAreaInsets ?? .zero
    }
    
}
