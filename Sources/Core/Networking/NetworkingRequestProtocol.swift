//
//  NetworkingRequestProtocol.swift
//  
//
//  Created by Luis Emilio Dias Wolf on 11/03/21.
//

import Foundation

public protocol NetworkingRequestProtocol {
    
    var method: NetworkingMethod { get }
    
    var url: String { get }
    
    var headers: [String: String]? { get }
    
    var parameters: [String: Any]? { get }
    
}

// MARK: - Default
public extension NetworkingRequestProtocol {
    
    var headers: [String: String]? { nil }
    
    var parameters: [String: Any]? { nil }
    
}
