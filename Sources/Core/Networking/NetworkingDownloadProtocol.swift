//
//  NetworkingDownloadProtocol.swift
//  
//
//  Created by Luis Emilio Dias Wolf on 14/03/21.
//

import Foundation

public protocol NetworkingDownloadRequestProtocol {
    
    var url: String { get }
    
    var contentType: [String] { get }
    
}
