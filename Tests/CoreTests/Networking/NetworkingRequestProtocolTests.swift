//
//  NetworkingRequestProtocolTests.swift
//  
//
//  Created by Luis Emilio Dias Wolf on 20/03/21.
//

import XCTest
@testable import Core

final class NetworkingRequestProtocolTests: XCTestCase {
    
    func test_defaultImplementation() {
        let sut = NetworkingRequestMock()
        XCTAssertTrue(sut.method == .get)
        XCTAssertTrue(sut.url == String())
        XCTAssertNil(sut.headers)
        XCTAssertNil(sut.parameters)
    }
    
}

extension NetworkingRequestProtocolTests {
    
    class NetworkingRequestMock: NetworkingRequestProtocol {
        var method: NetworkingMethod { .get }
        var url: String { String() }
    }
    
}
