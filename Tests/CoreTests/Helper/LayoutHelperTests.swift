//
//  LayoutHelperTests.swift
//  
//
//  Created by Luis Emilio Dias Wolf on 27/03/21.
//

import XCTest
@testable import Core

@available(iOS 10.0, *)
final class LayoutHelperTests: XCTestCase {

    func test_safeArea() {
        XCTAssertEqual(LayoutHelper.safeArea, .zero)
    }
    
}
