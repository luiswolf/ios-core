//
//  ColorsTest.swift
//  
//
//  Created by Luis Emilio Dias Wolf on 27/03/21.
//

import XCTest
@testable import Core

final class ColorsTest: XCTestCase {
    
    func test_invalidColor() {
        XCTAssertNil(UIColor.getColor(named: "Invalid"))
    }
    
    func test_validColors() {
        XCTAssertNotNil(UIColor.getColor(named: "Black"))
        XCTAssertNotNil(UIColor.getColor(named: "White"))
        XCTAssertNotNil(UIColor.getColor(named: "Orange"))
        XCTAssertNotNil(UIColor.getColor(named: "Gray"))
    }
    
}
