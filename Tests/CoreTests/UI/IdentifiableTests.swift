//
//  IdentifiableTests.swift
//  
//
//  Created by Luis Emilio Dias Wolf on 25/03/21.
//

import XCTest
@testable import Core

final class IdentifiableTests: XCTestCase {
    
    class TestTableViewCell: UITableViewCell, Identifiable {}
    
    func test_identifier_hasSameNameThatClass() {
        XCTAssertEqual(TestTableViewCell.identifier, "TestTableViewCell")
    }
    
}
